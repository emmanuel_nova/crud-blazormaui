﻿

namespace MAUICrudP3.Data
{
    using MAUICrudP3.Models;
    using MAUICrudP3.Interfaces;
    using Microsoft.Azure.Cosmos;

    public class DBCosmosAzureService : IDBCosmosAzure
    {
        public DBCosmosAzureService()
        {
            Constans.cosmosClient = new CosmosClient
                (Constans.EndPointUri, Constans.PrimaryKey);
        }

        public async Task CreateDatabaseAsync()
        {
            try
            {
                Constans.database = await Constans.cosmosClient
                    .CreateDatabaseIfNotExistsAsync(Constans.databaseId);
            }
            catch (CosmosException ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task CreateCollectionMoviesAsync()
        {
            try
            {

                Constans.container =
                    await Constans.database.CreateContainerIfNotExistsAsync(Constans.containerId, "/partitionKey");

            }
            catch (CosmosException ex)
            {
                throw new Exception(ex.Message);
            }

        }


        public async Task DeleteMovieAsync(string id, string partitionKey)
        {
            try
            {
                ItemResponse<Movie> itemResponse = await Constans.container.DeleteItemAsync<Movie>(id, new PartitionKey(partitionKey));
            }
            catch (CosmosException ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task EditMovie(Movie movie)
        {
            try
            {

                ItemResponse<Movie> itemResponse = await Constans.container.ReadItemAsync<Movie>(movie.Id, new PartitionKey(movie.PartitionKey));
                itemResponse = await Constans.container.ReplaceItemAsync<Movie>(movie,movie.Id,new PartitionKey(movie.PartitionKey));
            }
            catch (CosmosException ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<Movie> FindMovieAsync(string id, string partitionKey)
        {
            try
            {

                ItemResponse<Movie> itemResponse = await Constans.container.ReadItemAsync<Movie>(id, new PartitionKey(partitionKey));
                return itemResponse;

            }
            catch (CosmosException ex)
            {
                return null;
                throw new Exception(ex.Message);
            }

           
        }

        public Task<List<Movie>> FindMoviesAsync(string category)
        {
            throw new NotImplementedException();
        }

        public async  Task<List<Movie>> GetMoviesAsync()
        {
            List<Movie> movies = new List<Movie>();
            try
            {
                var sqlQuery = "SELECT * FROM c WHERE c.partitionKey = 'Peliculas'";
                QueryDefinition queryDefinition = new QueryDefinition(sqlQuery);
                FeedIterator<Movie> queryResultSetIterator = Constans.container.
                       GetItemQueryIterator<Movie>(queryDefinition);
                while (queryResultSetIterator.HasMoreResults)
                {
                    FeedResponse<Movie> currentResultSet = await queryResultSetIterator.ReadNextAsync();
                    foreach (var item in currentResultSet)
                    {
                        movies.Add(item);
                    }
                } 
            }catch (CosmosException ex){
                throw new Exception(ex.Message);
            }

            return movies;
        }

        public async Task InsertMovieAsync(Movie newMovie)
        {
            try
            {
                ItemResponse<Movie> itemResponse = await Constans.container.CreateItemAsync(newMovie, new PartitionKey(newMovie.PartitionKey));
            }
            catch (CosmosException ex)
            {
                throw new Exception(ex.Message);
            }
        }


    }
}
