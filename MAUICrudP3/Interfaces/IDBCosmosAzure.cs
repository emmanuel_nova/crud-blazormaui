﻿using MAUICrudP3.Models;

namespace MAUICrudP3.Interfaces
{
    public interface IDBCosmosAzure
    {
        Task CreateDatabaseAsync();
        Task CreateCollectionMoviesAsync();
        Task<List<Movie>> GetMoviesAsync();
        Task InsertMovieAsync(Movie movie);
        Task<Movie> FindMovieAsync(string id, string partitionKey);
        Task EditMovie(Movie movie);
        Task DeleteMovieAsync(string id,string partitionKey);
        Task<List<Movie>> FindMoviesAsync(string category);


    }
}
